var url_final = 'http://vc-api.dew';
var dominio_actual = 'http://vc.dew';


$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('token'));
    }
});

function cargarTabla() {
    $('#tableUsers td').remove();
    $.ajax(url_final + '/api/administrator/user', {
        method: 'GET',
        dataType: 'JSON',
        success: function(resp) {
            if (resp.user.length != 0) {
                for (let i = 0; i < resp.user.length; i++) {
                    $('#tableUsers').append("<tr><td>" + resp.user[i].name + " " + resp.user[i].apepat + " " + resp.user[i].apemat + "</td><td>" + resp.user[i].rut + "</td><td>" + resp.user[i].email + "</td><td>" + resp.user[i].department_id + "</td><td><button class='btn btn-primary' data-attr=" + resp.user[i].id + " data-toggle='modal' title='Editar' id='editUser' data-target='#ModalEdit'><span class='icon'><i class='fa fa-user-edit' aria-hidden='true'></i></span></button></td><td><button class='btn btn-danger' id='deleteUser' title='Eliminar' data-attr=" + resp.user[i].id + "><span class='icon'><i class='fa fa-trash' aria-hidden='true'></i></span></button></td></tr>");
                }
            }

        },
        error: function() {

        }

    });

}

$(document).ready(function() {

    $.ajax(url_final + '/api/administrator/role/listar', {
        method: 'GET',
        success: function(resp) {
            if (resp.roles.length != 0) {
                for (let i = 0; i < resp.roles.length; i++) {
                    $('#FormCreate select[id=role_id]').append('<option value=' + resp.roles[i].id + '>' + resp.roles[i].type + '</option>')
                    $('#FormEdit select[id=role_id]').append('<option value=' + resp.roles[i].id + '>' + resp.roles[i].type + '</option>')
                }
            }
        }
    });

    $.ajax(url_final + '/api/administrator/department/listar', {
        method: 'GET',
        success: function(resp) {
            if (resp.departments.length != 0) {
                for (let i = 0; i < resp.departments.length; i++) {
                    $('#FormCreate select[id=department_id]').append('<option value=' + resp.departments[i].id + '>' + resp.departments[i].name + '</option>')
                    $('#FormEdit select[id=department_id]').append('<option value=' + resp.departments[i].id + '>' + resp.departments[i].name + '</option>')

                }
            }
        }
    });



    cargarTabla();
    // var tableRecover = $('#tableUsers').DataTable({
    //     language: {
    //         emptyTable: "No hay datos disponibles",
    //         info: "Mostrando _START_ a _END_ de _TOTAL_ entradas",
    //         infoEmpty: "Mostrando 0 a 0 de 0 entradas",
    //         infoFiltered: "(Filtrado de _MAX_ entradas totales)",
    //         lengthMenu: "Mostrar _MENU_ entradas",
    //         loadingRecords: "Cargando",
    //         processing: "Procesando",
    //         search: "Buscar",
    //         zeroRecords: "No hay registros eliminados",
    //         paginate: {
    //             first: "PRIMERA",
    //             last: "ULTIMA",
    //             next: "SIGUIENTE",
    //             previous: "ANTERIOR"
    //         }
    //     },
    //     displayLength: 10,
    //     processing: true,
    //     serverSide: false,
    //     responsive: true,
    //     autoWidth: false,
    //     ajax: {
    //         url: url_final + '/api/administrator/user/',
    //         error: function(xhr, error, thrown) {

    //         }
    //     },
    //     "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    //         console.log(aData);
    //         console.log(nRow);
    //         $(nRow).html("<td>" + aData.name + " " + aData.apepat + " " + aData.apemat + "</td><td>" + aData.rut + "</td><td>" + aData.email + "</td><td>" + aData.department_id + "</td><td><button class='btn btn-primary' data-attr=" + aData.id + " data-toggle='modal' title='Editar' id='editUser' data-target='#ModalEdit'><span class='icon'><i class='fa fa-user-edit' aria-hidden='true'></i></span></button></td><td><button class='btn btn-danger' id='deleteUser' title='Eliminar' data-attr=" + aData.id + "><span class='icon'><i class='fa fa-trash' aria-hidden='true'></i></span></button></td>");
    //     },
    //     columns: [
    //         { data: 'name', name: 'Nombre' },
    //         { data: 'apepat', name: 'Apellido Paterno' },
    //         { data: 'apemat', name: 'Apellido Materno' },
    //         { data: 'rut', name: 'Rut' },
    //         {
    //             targets: -1,
    //             data: null,
    //             orderable: false,
    //             searchable: false,
    //             defaultContent: "...cargando"

    //         }
    //     ],
    // });

});

$(document).on('submit', '#FormCreate', function() {
    event.preventDefault();
    $('.btn').attr('disabled', true);
    var formData = new FormData($(this)[0]);
    $.ajax(url_final + '/api/administrator/user', {
        method: "POST",
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function() {
            cargarTabla();
            document.getElementById("FormCreate").reset();
            $('.btn').attr('disabled', false);
            swal('Usuario Creado!', '', 'success');
            $('.modal').modal('hide');
        },
        error: function() {
            $('.btn').attr('disabled', false);
            swal('Error al crear usuario!', 'verificar campos', 'error');
        }

    });

});

$(document).on('click', '#editUser', function() {
    var id = $(this).attr('data-attr');
    $.ajax(url_final + '/api/administrator/user/' + id, {
        method: "GET",
        dataType: "JSON",
        success: function(resp) {
            $('#FormEdit input[id=user_id]').val(id);
            $('#FormEdit input[id=name]').val(resp.user.name);
            $('#FormEdit input[id=apepat]').val(resp.user.apepat);
            $('#FormEdit input[id=apemat]').val(resp.user.apemat);
            $('#FormEdit input[id=rut]').val(resp.user.rut);
            $('#FormEdit input[id=email]').val(resp.user.email);
            $('#FormEdit select[id=role_id]').val(resp.user.role_id);
            $('#FormEdit select[id=department_id]').val(resp.user.department_id);
        }

    });

});

$(document).on('submit', '#FormEdit', function() {
    event.preventDefault();
    $('.btn').attr('disabled', true);
    var formData = new FormData($(this)[0]);
    console.log(formData);
    $.ajax(url_final + '/api/administrator/user/' + $('#FormEdit input[id=user_id]').val(), {
        method: "POST",
        headers: { "X-HTTP-Method-Override": "PUT" },
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function() {
            cargarTabla();
            document.getElementById("FormEdit").reset();
            $('.btn').attr('disabled', false);
            swal('Usuario Editado!', '', 'success');
            $('.modal').modal('hide');
        },
        error: function(error) {
            $('.btn').attr('disabled', false);
            swal('Error al crear usuario!', 'verificar campos', 'error');
        }

    });

});

$(document).on('click', '#deleteUser', function() {
    event.preventDefault();
    var id = $(this).attr('data-attr');
    swal({
            title: "¿Esta seguro?",
            text: "Si eliminara al usuario seleccionado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax(url_final + '/api/administrator/user/' + id, {
                    method: 'DELETE',
                    success: function() {
                        cargarTabla();
                        swal('Usuario Eliminado', '', 'success');
                    }
                });
            } else {}
        });

});

$("#salir").click(function() {
    swal({
            title: "¿Esta seguro?",
            text: "Si continua se cerrara la sesion",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Salir!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                localStorage.clear();
                $(location).attr('href', dominio_actual);
            } else {}
        });

});