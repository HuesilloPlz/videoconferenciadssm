var url_final = 'http://vc-api.dew';


document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
        },
        selectable: true,
        locale: 'es',
        editable: true,
        eventClick: function(info) {
            $.ajax(url_final + '/api/videoconferences/approved_by_id/' + info.event.id, {
                method: 'GET',
                dataType: 'JSON',
                success: function(resp) {
                    console.log(resp);
                    $('#ModalDetail input[id=title]').val(resp.vc[0].title);
                    $('#ModalDetail input[id=id]').val(resp.vc[0].id);
                    $('#ModalDetail textarea[id=descriptions]').val(resp.vc[0].descriptions);
                    $('#ModalDetail input[id=start_date]').val(resp.vc[0].start_date);
                    $('#ModalDetail input[id=people_cant]').val(resp.vc[0].people_cant);
                    $('#ModalDetail input[id=end_date]').val(resp.vc[0].end_date);
                    $('#ModalDetail input[id=start_time]').val(resp.vc[0].start_time);
                    $('#ModalDetail input[id=end_time]').val(resp.vc[0].end_time);
                    $('#ModalDetail input[id=user_id]').val(resp.vc[0].user.name + ' ' + resp.vc[0].user.apepat + ' ' + resp.vc[0].user.apemat);
                    $('#ModalDetail input[id=department]').val(resp.vc[0].user.department.name);

                },
                error: function(error) {
                    console.log(error);
                }

            });

            $('#ModalDetail').modal('show');

        }


    });

    $.ajax(url_final + '/api/videoconferences/approved', {
        method: 'GET',
        success: function(resp) {
            console.log(resp.vc.length);
            for (var i = 0; i < resp.vc.length; i++) {

                calendar.addEvent({
                    id: resp.vc[i].id,
                    title: resp.vc[i].title,
                    start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                    end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                    textColor: '#17202A',
                    color: '#2ECC71',
                });

            }
            calendar.refetchEvents();

        }
    });

    calendar.render();
});