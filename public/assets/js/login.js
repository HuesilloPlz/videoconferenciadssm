//Variables de sistema
var url_final = 'http://vc-api.dew';
var dominio_actual = 'http://vc.dew';
$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('token'));
    }
});
$(document).ajaxSuccess(function(event, request, setting) {
    if (request.status == 200) {}
});
$(document).ajaxError(function(event, request, setting) {
    console.log(request.status);
    if (request.status == 0) {
        swal("Error interno de servidor", "", "error");
        console.log("No Conectado: verificar su Red / Access-Control-Request-Methods or Access-Denied [0]");
    } else if (request.status == 404) {
        swal("Error de usuario o contraseña", "", "error");
        console.log("Pagina no encontrada [404]");
    } else if (request.status == 401) {
        swal("Error de usuario o contraseña", "", "error");
    } else if (request.status == 500) {
        swal("Error interno en el servidor", "", "error");
        console.log("Error interno en el servidor [500]");
        $(location).attr('href', dominio_actual);
    } else if (request.status == 400) {
        console.log("Token no Disponible [400]");
        $(location).attr('href', dominio_actual);
    } else if (request.status == 501) {
        console.log("no hay acceso 501");
        $(location).attr('href', dominio_actual);
    }
});



$(document).ready(function() {
    //Ingreso de datos para obtener token
    $("#login").click(function() {
        var email = $("#email").val();
        var password = $("#password").val();
        obtenerToken(email, password);
        $('form').keypress(function(e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
    });

    //Funcion principal para obtencion de token
    function obtenerToken(email, password) {
        $.ajax({
            url: url_final + '/api/auth',
            method: 'POST',
            data: {
                email: email,
                password: password,
                grant_type: 'password'
            },
            error: function(e) {
                console.log(e);
            },
            success: function(response) {
                console.log(response);
                localStorage.setItem('token', response.token);
                var decoded = jwt_decode(localStorage.getItem('token'));
                localStorage.setItem('user', decoded.datos.name);
                localStorage.setItem('apepat', decoded.datos.apepat);
                localStorage.setItem('apemat', decoded.datos.apemat);
                localStorage.setItem('mail', decoded.datos.email);
                localStorage.setItem('rol', decoded.datos.role_id);
                localStorage.setItem('rut', decoded.datos.rut);
                localStorage.setItem('id', decoded.datos.id);
                console.log(decoded.datos.role_id);
                (localStorage.getItem('token'));
                if (decoded.datos.role_id == 1) {
                    $(location).attr('href', dominio_actual + '/administrador/inicio');
                } else if (decoded.datos.role_id == 2) {
                    $(location).attr('href', dominio_actual + '/administrativo/inicio');
                }
            }
        });
    }
});
jQuery(function($) {
    $(document).on('click', '.toolbar a[data-target]', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible'); //hide others
        $(target).addClass('visible'); //show target
    });
});



//you don't need this, just used for changing background
jQuery(function($) {

    $('#btn-login-dark').on('click', function(e) {
        $('body').attr('class', 'login-layout');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'blue');
        e.preventDefault();
    });

    $('#btn-login-light').on('click', function(e) {
        $('body').attr('class', 'login-layout light-login');
        $('#id-text2').attr('class', 'grey');
        $('#id-company-text').attr('class', 'blue');
        e.preventDefault();
    });

    $('#btn-login-blur').on('click', function(e) {
        $('body').attr('class', 'login-layout blur-login');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'light-blue');
        e.preventDefault();
    });

});