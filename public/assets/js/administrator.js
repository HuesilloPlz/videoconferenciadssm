var url_final = 'http://vc-api.dew';
var dominio_actual = 'http://vc.dew';


$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('token'));
    }
});


document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
        },
        selectable: true,
        locale: 'es',
        editable: true,
        eventClick: function(info) {
            console.log(info.event.id);
            $.ajax(url_final + '/api/administrator/videoconferences/' + info.event.id, {
                method: 'GET',
                dataType: 'JSON',
                success: function(resp) {
                    console.log(resp.vc.user)
                    $('#ModalState input[id=title]').val(resp.vc.title);
                    $('#ModalState input[id=id]').val(resp.vc.id);
                    $('#ModalState textarea[id=descriptions]').val(resp.vc.descriptions);
                    $('#ModalState input[id=start_date]').val(resp.vc.start_date);
                    $('#ModalState input[id=people_cant]').val(resp.vc.people_cant);
                    $('#ModalState input[id=end_date]').val(resp.vc.end_date);
                    $('#ModalState input[id=start_time]').val(resp.vc.start_time);
                    $('#ModalState input[id=end_time]').val(resp.vc.end_time);
                    $('#ModalState input[id=user_id]').val(resp.vc.user_id);
                    $('#ModalState input[id=userName]').val(resp.vc.user.name + ' ' + resp.vc.user.apepat + ' ' + resp.vc.user.apemat);
                    $('#ModalState input[id=department]').val(resp.vc.user.department.name);
                    $('#ModalState select[id=state]').val(resp.vc.state);
                }
            });

            $('#ModalState').modal('show');

        },
        select: function(info) {
            var fechaInicio = new Date(info.start);
            var fechaFinal = new Date(info.end);
            var formateDateInicio = fechaInicio.getFullYear() + "-" + ((fechaInicio.getMonth() + 1) < 10 ? '0' + (fechaInicio.getMonth() + 1) : (fechaInicio.getMonth() + 1)) + "-" + (fechaInicio.getDate() < 10 ? '0' + fechaInicio.getDate() : fechaInicio.getDate());
            // var formateDateFinal=fechaFinal.getFullYear() + "-" + ((fechaFinal.getMonth() + 1) < 10 ? '0' + (fechaFinal.getMonth() + 1) : (fechaFinal.getMonth() + 1)) + "-" + (fechaFinal.getDate() < 10 ? '0' + fechaFinal.getDate() : fechaFinal.getDate());
            console.log(formateDateInicio);
            $('#user_id').val(localStorage.getItem('id'));
            $('#start_date').val(formateDateInicio);
            $('#end_date').val(formateDateInicio);
            $('#ModalCreate').modal('show');
        }

    });


    $(document).on('click', '#changeState', function() {
        var id = $('#ModalState input[id=id]').val();
        var title = $('#ModalState input[id=title]').val();
        var descriptions = $('#ModalState textarea[id=descriptions]').val();
        var people_cant = $('#ModalState input[id=people_cant]').val();
        var start_date = $('#ModalState input[id=start_date]').val();
        var end_date = $('#ModalState input[id=end_date]').val();
        var start_time = $('#ModalState input[id=start_time]').val();
        var end_time = $('#ModalState input[id=end_time]').val();
        var user_id = $('#ModalState input[id=user_id]').val();
        var stado = $('#ModalState select[id=state]').val();
        $.ajax(url_final + '/api/administrator/videoconferences/' + id, {
            method: "PUT",
            dataType: "JSON",
            data: {
                title: title,
                people_cant: people_cant,
                descriptions: descriptions,
                start_date: start_date,
                end_date: end_date,
                start_time: start_time,
                end_time: end_time,
                user_id: user_id,
                state: stado
            },
            success: function(resp) {
                console.log(resp);
                if (resp.vc.state == 'aprobado') {
                    const event = calendar.getEventById(resp.vc.id);
                    event.setProp('color', '#2ECC71');
                    event.setProp('textColor', '##17202');

                    $('#imageToast').attr('src', '/assets/images/box-green.jpg');
                    $('.toast-body').html('El estado de la video cambio a Aprobado');
                    $('.toast').toast('show');
                    $('#ModalState').modal('hide');
                    return false

                } else if (resp.vc.state == 'solicitado') {
                    const event = calendar.getEventById(resp.vc.id);
                    event.setProp('color', '#F1C40F');
                    event.setProp('textColor', '##17202');

                    $('#imageToast').attr('src', '/assets/images/box-yellow.jpg');
                    $('.toast-body').html('El estado de la video cambio a Solicitado');
                    $('.toast').toast('show');
                    $('#ModalState').modal('hide');
                    return false
                } else if (resp.vc.state == 'rechazado') {
                    const event = calendar.getEventById(resp.vc.id);
                    event.setProp('color', '#E74C3C');
                    event.setProp('textColor', '#FDFEFE');

                    $('#imageToast').attr('src', '/assets/images/box-red.png');
                    $('.toast-body').html('El estado de la video cambio a Rechazado');
                    $('.toast').toast('show');
                    $('#ModalState').modal('hide');
                    return false
                } else if (resp.vc.state == 'suspendido') {
                    const event = calendar.getEventById(resp.vc.id);
                    event.setProp('color', '#17202A');
                    event.setProp('textColor', '#FDFEFE');

                    $('#imageToast').attr('src', '/assets/images/box-black.png');
                    $('.toast-body').html('El estado de la video cambio a Suspendido');
                    $('.toast').toast('show');
                    $('#ModalState').modal('hide');
                    return false
                }
                document.getElementById('dataEvent').reset();

            }
        });


    });

    $.ajax(url_final + '/api/administrator/videoconferences/', {
        method: 'GET',
        success: function(resp) {
            console.log(resp.vc.length);
            for (var i = 0; i < resp.vc.length; i++) {
                if (resp.vc[i].state == 'solicitado') {
                    calendar.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#17202A',
                        color: '#F1C40F',
                    });
                } else if (resp.vc[i].state == 'aprobado') {
                    calendar.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#17202A',
                        color: '#2ECC71',
                    });
                } else if (resp.vc[i].state == 'rechazado') {
                    calendar.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#FDFEFE',
                        color: '#E74C3C',
                    });
                } else if (resp.vc[i].state == 'suspendido') {
                    calendar.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#FDFEFE',
                        color: '#17202A',
                    });
                }

            }
            calendar.refetchEvents();

        }
    });


    $(document).on('submit', '#FormCreate', function(event) {
        event.preventDefault();
        $('.btn-primary').attr('disabled', true);
        // const formData = new FormData(this);
        const title = $('#FormCreate input[id=title]').val();
        const people_cant = $('#FormCreate input[id=people_cant]').val();
        const descriptions = $('#FormCreate textarea[id=descriptions]').val();
        const start_date = $('#FormCreate input[id=start_date]').val();
        const end_date = $('#FormCreate input[id=end_date]').val();
        const start_time = $('#FormCreate input[id=start_time]').val();
        const end_time = $('#FormCreate input[id=end_time]').val();
        const user_id = $('#FormCreate input[id=user_id]').val();
        $.ajax(url_final + '/api/administrator/videoconferences', {
            method: "POST",
            dataType: "JSON",
            data: {
                title: title,
                descriptions: descriptions,
                people_cant: people_cant,
                start_date: start_date,
                end_date: end_date,
                start_time: start_time,
                end_time: end_time,
                user_id: user_id,
                state: 'solicitado'
            },
            success: function(resp) {
                console.log(resp, 'algo');
                calendar.addEvent({
                    id: resp.vc.id,
                    title: title,
                    start: start_date + 'T' + start_time,
                    end: end_date + 'T' + end_time,
                    textColor: '#17202A',
                    color: '#F1C40F',
                });
                calendar.refetchEvents();
                $('.btn-primary').attr('disabled', false);
                $(".modal").modal('hide');
                swal("Agregado!", "La Video Fue Agendada Con Exito", "success");

            },
            error: function() {
                $('.btn-primary').attr('disabled', false);
                swal("Error", "Video No Agendada, verificar datos plz", "error");
            }
        });

    });




    calendar.render();
});



$(document).ready(function() {


    $.ajax(url_final + '/api/administrator/user/' + localStorage.getItem('id'), {
        method: 'GET',
        success: function(resp) {
            $('#username').text(resp.user.name + ' ' + resp.user.apepat + ' ' + resp.user.apemat);
            $('#useremail').text(resp.user.email);
            $('#userrut').html(resp.user.rut);

        }

    });

});


$("#salir").click(function() {
    swal({
            title: "¿Esta seguro?",
            text: "Si continua se cerrara la sesion",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Salir!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                localStorage.clear();
                $(location).attr('href', dominio_actual);
            } else {}
        });

});