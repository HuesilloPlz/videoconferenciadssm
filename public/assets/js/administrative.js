var url_final = 'http://vc-api.dew';
var dominio_actual = 'http://vc.dew';


$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('token'));
    }
});


document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar2 = document.getElementById('calendar2');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
        },
        selectable: true,
        locale: 'es',
        editable: true,
        eventClick: function(info) {
            $.ajax(url_final + '/api/administrative/videoconferences/' + info.event.id, {
                method: 'GET',
                dataType: 'JSON',
                success: function(resp) {
                    $('#ModalDetail input[id=title]').val(resp.vc.title);
                    $('#ModalDetail input[id=id]').val(resp.vc.id);
                    $('#ModalDetail textarea[id=descriptions]').val(resp.vc.descriptions);
                    $('#ModalDetail input[id=start_date]').val(resp.vc.start_date);
                    $('#ModalDetail input[id=people_cant]').val(resp.vc.people_cant);
                    $('#ModalDetail input[id=end_date]').val(resp.vc.end_date);
                    $('#ModalDetail input[id=start_time]').val(resp.vc.start_time);
                    $('#ModalDetail input[id=end_time]').val(resp.vc.end_time);
                    $('#ModalDetail input[id=user_id]').val(resp.vc.user.name + ' ' + resp.vc.user.apepat + ' ' + resp.vc.user.apemat);
                }
            });

            $('#ModalDetail').modal('show');

            $(document).on('click', '#deleteUser', function() {
                const id = $('#ModalDetail input[id=id]').val();
                swal({
                        title: "¿Esta seguro?",
                        text: "Seguro eliminara la video?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        cancelButtonText: "Cancelar!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax(url_final + '/api/administrative/videoconferences/' + id, {
                                method: 'DELETE',
                                success: function(resp) {
                                    info.event.remove();
                                    $('#ModalDetail').modal('hide');
                                    swal('Eliminado!', 'La Video Fue Eliminada del Calendario con Exito', 'success');
                                }
                            });
                        }
                    });
            });

        },
        select: function(info) {
            var fechaInicio = new Date(info.start);
            var fechaFinal = new Date(info.end);
            var formateDateInicio = fechaInicio.getFullYear() + "-" + ((fechaInicio.getMonth() + 1) < 10 ? '0' + (fechaInicio.getMonth() + 1) : (fechaInicio.getMonth() + 1)) + "-" + (fechaInicio.getDate() < 10 ? '0' + fechaInicio.getDate() : fechaInicio.getDate());
            // var formateDateFinal=fechaFinal.getFullYear() + "-" + ((fechaFinal.getMonth() + 1) < 10 ? '0' + (fechaFinal.getMonth() + 1) : (fechaFinal.getMonth() + 1)) + "-" + (fechaFinal.getDate() < 10 ? '0' + fechaFinal.getDate() : fechaFinal.getDate());
            console.log(formateDateInicio);
            $('#user_id').val(localStorage.getItem('id'));
            $('#start_date').val(formateDateInicio);
            $('#end_date').val(formateDateInicio);
            $('#ModalCreate').modal('show');
        }

    });

    var calendarNew = new FullCalendar.Calendar(calendar2, {
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
        },
        selectable: true,
        locale: 'es',
        editable: true,
        eventClick: function(info) {
            $.ajax(url_final + '/api/videoconferences/approved_by_id/' + info.event.id, {
                method: 'GET',
                dataType: 'JSON',
                success: function(resp) {
                    $('#ModalDetailAll input[id=title]').val(resp.vc[0].title);
                    $('#ModalDetailAll input[id=id]').val(resp.vc[0].id);
                    $('#ModalDetailAll textarea[id=descriptions]').val(resp.vc[0].descriptions);
                    $('#ModalDetailAll input[id=start_date]').val(resp.vc[0].start_date);
                    $('#ModalDetailAll input[id=people_cant]').val(resp.vc[0].people_cant);
                    $('#ModalDetailAll input[id=end_date]').val(resp.vc[0].end_date);
                    $('#ModalDetailAll input[id=start_time]').val(resp.vc[0].start_time);
                    $('#ModalDetailAll input[id=end_time]').val(resp.vc[0].end_time);
                    $('#ModalDetailAll input[id=user_id]').val(resp.vc[0].user.name + ' ' + resp.vc[0].user.apepat + ' ' + resp.vc[0].user.apemat);
                    $('#ModalDetailAll input[id=department]').val(resp.vc[0].user.department.name);

                }
            });
            $('#ModalDetailAll').modal('show');
        }

    });

    //CARGA PARA EL CALENDARIO DEL DEPARTAMENTO
    $.ajax(url_final + '/api/administrative/user/' + localStorage.getItem('id'), {
        method: 'GET',
        dataType: 'JSON',
        success: function(resp) {
            var id = resp.user.department_id;
            $.ajax(url_final + '/api/administrative/videoconferences/find_by_department_id/' + id, {
                method: 'GET',
                success: function(resp) {
                    console.log(resp.vc.length);
                    for (var i = 0; i < resp.vc.length; i++) {
                        if (resp.vc[i].state == 'solicitado') {
                            calendar.addEvent({
                                id: resp.vc[i].id,
                                title: resp.vc[i].title,
                                start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                                end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                                textColor: '#17202A',
                                color: '#F1C40F',
                            });
                        } else if (resp.vc[i].state == 'rechazado') {
                            calendar.addEvent({
                                id: resp.vc[i].id,
                                title: resp.vc[i].title,
                                start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                                end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                                textColor: '#FDFEFE',
                                color: '#E74C3C',
                            });
                        } else if (resp.vc[i].state == 'aprobado') {
                            calendar.addEvent({
                                id: resp.vc[i].id,
                                title: resp.vc[i].title,
                                start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                                end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                                textColor: '#17202A',
                                color: '#2ECC71',
                            });
                        } else if (resp.vc[i].state == 'suspendido') {
                            calendar.addEvent({
                                id: resp.vc[i].id,
                                title: resp.vc[i].title,
                                start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                                end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                                textColor: '#FDFEFE',
                                color: '#17202A',
                            });
                        }

                    }
                    calendar.refetchEvents();

                }
            });
        }
    });


    //CARGA PARA EL CALENDARIO GLOBAL

    $.ajax(url_final + '/api/videoconferences/approved', {
        method: 'GET',
        success: function(resp) {
            console.log(resp.vc.length);
            for (var i = 0; i < resp.vc.length; i++) {
                if (resp.vc[i].state == 'solicitado') {
                    calendarNew.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#17202A',
                        color: '#F1C40F',
                    });
                } else if (resp.vc[i].state == 'rechazado') {
                    calendarNew.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#FDFEFE',
                        color: '#E74C3C',
                    });
                } else if (resp.vc[i].state == 'aprobado') {
                    calendarNew.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#17202A',
                        color: '#2ECC71',
                    });
                } else if (resp.vc[i].state == 'suspendido') {
                    calendarNew.addEvent({
                        id: resp.vc[i].id,
                        title: resp.vc[i].title,
                        start: resp.vc[i].start_date + 'T' + resp.vc[i].start_time,
                        end: resp.vc[i].end_date + 'T' + resp.vc[i].end_time,
                        textColor: '#FDFEFE',
                        color: '#17202A',
                    });
                }

            }
            calendarNew.refetchEvents();

        }
    });




    $(document).on('submit', '#FormCreate', function(event) {
        event.preventDefault();
        $('.btn-primary').attr('disabled', true);
        // const formData = new FormData(this);
        const title = $('#FormCreate input[id=title]').val();
        const descriptions = $('#FormCreate textarea[id=descriptions]').val();
        const people_cant = $('#FormCreate input[id=people_cant]').val();
        const start_date = $('#FormCreate input[id=start_date]').val();
        const end_date = $('#FormCreate input[id=end_date]').val();
        const start_time = $('#FormCreate input[id=start_time]').val();
        const end_time = $('#FormCreate input[id=end_time]').val();
        const user_id = $('#FormCreate input[id=user_id]').val();
        console.log(title, descriptions, start_date, end_date, start_time, end_time, user_id);
        $.ajax(url_final + '/api/administrative/videoconferences', {
            method: "POST",
            dataType: "JSON",
            data: {
                title: title,
                descriptions: descriptions,
                people_cant: people_cant,
                start_date: start_date,
                end_date: end_date,
                start_time: start_time,
                end_time: end_time,
                user_id: user_id,
                state: 'solicitado',

            },
            success: function(resp) {
                console.log(resp, 'algo');
                calendar.addEvent({
                    id: resp.vc.id,
                    title: title,
                    start: start_date + 'T' + start_time,
                    end: end_date + 'T' + end_time,
                    textColor: '#17202A',
                    color: '#F1C40F',
                });
                calendar.refetchEvents();
                $('.btn-primary').attr('disabled', false);
                $(".modal").modal('hide');
                swal("Agregado!", "La Video Fue Agendada Con Exito", "success");

            },
            error: function() {
                $('.btn-primary').attr('disabled', false);
                swal("Error", "Video No Agendada, verificar datos plz", "error");
            }
        });

    });



    calendarNew.render();
    calendar.render();

    $(document).on('click', '#home-tab', function() {
        calendar.render();
    });
    $(document).on('click', '#fullcalendar-tab', function() {
        calendarNew.render();
    });

});



$(document).ready(function() {


    $.ajax(url_final + '/api/administrative/user/' + localStorage.getItem('id'), {
        method: 'GET',
        success: function(resp) {
            $('#username').text(resp.user.name + ' ' + resp.user.apepat);
        }

    });

});


$("#salir").click(function() {
    swal({
            title: "¿Esta seguro?",
            text: "Si continua se cerrara la sesion",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Salir!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                localStorage.clear();
                $(location).attr('href', dominio_actual);
            } else {}
        });

});