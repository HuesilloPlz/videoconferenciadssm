<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>

    <link rel="stylesheet" href="/assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link href='/assets/fullcalendar/packages/core/main.min.css' rel='stylesheet' />
    <link href='/assets/fullcalendar/packages/daygrid/main.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="/assets/fullcalendar/packages/timegrid/main.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">

</head>
<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg  navbar-dark bg-primary">   
    <b><label id="username" style="color:white;">Invitado</label></b>    
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#"> <b>VIDEOCONFERENCIA DSSM</b> <span class="sr-only">(current)</span></a>
                </li>     
                <li class="nav-item">
                        <a class="btn btn-danger" href='login'>Iniciar Sesion</a>
                </li>     
            </ul>                        
    </nav>
   <!--  -->
    
    <!-- CONTENT -->        
    <br>
    <div class="container">
        <div class="row">            
            <div class="col-12">
                        <div class="card p-4 shadow-lg p-3 bg-white">
                                <div id="calendar"></div>
                        </div>                                                
            </div>
        </div>
    </div>

    @include('detailInvitado')
    <!--  -->

    <!-- SCRIPTS -->
    <script src="/assets/Jquery/jquery-v341.js"></script>
    <script src="/assets/jwt-decode.min.js"></script>
    <script src="/assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script src='/assets/fullcalendar/packages/core/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/core/locales/es.js"></script>
    <script src='/assets/fullcalendar/packages/daygrid/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/timegrid/main.js"></script>
    <script src="/assets/fullcalendar/packages/interaction/main.min.js"></script>
    <script src="/assets/js/invitado.js"></script>   

    <!--  -->
</body>
</html>