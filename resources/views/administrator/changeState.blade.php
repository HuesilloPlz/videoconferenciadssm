<div class="modal fade" id="ModalState" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="descriptions" nmae="descriptions">Video Conferencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="dataEvent">
          <input type="hidden" id="id" name="id">
          <input type="hidden" id="user_id" name="user_id">
          <input type="hidden" class="form-control" name="end_date" id="end_date" >
            <div class="form-row">
              <div class="col-4">
                <label>Fecha</label>
                <input type="text" class="form-control" name="start_date" id="start_date" readonly>
              </div>
              <div class="col-4">
                <label>Hora de Inicio</label>
                <input type="text" class="form-control" name="start_time" id="start_time" readonly>
              </div>
              <div class="col-4">
                <label>Hora de Termino</label>
                <input type="text" class="form-control" name="end_time" id="end_time" readonly>
              </div>
            </div>
            <hr>
            <div class="form-row">
              <div class="col-3">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="title" readonly>
              </div>
              <div class="col-3">
                <label>Cantidad de personas</label>
                <input type="text" class="form-control" name="people_cant" id="people_cant" readonly>
              </div>
              <div class="col-3">
                <label>Responsable</label>
                <input type="text" class="form-control" name="userName" id="userName" readonly>
              </div>
              <div class="col-3">
                <label>Departamento</label>
                <input type="text" class="form-control" name="department" id="department" readonly>
              </div>
            </div>
            <hr>
            <div class="form-row">            
              <label>Descripcion</label>
              <textarea type="text" class="form-control" name="descriptions" id="descriptions" cols="30" rows="5" readonly></textarea>
            </div>
            <div class="form-row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Estado</label>
                            <select class="form-control" id="state" name="state" required>
                                <option value="aprobado">Aprobado</option>
                                <option value="solicitado">Solicitado</option>                        
                                <option value="rechazado">Rechazado</option>
                                <option value="suspendido">Suspendido</option>

                            </select>
                  </div>                       
        </form>          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="changeState" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>