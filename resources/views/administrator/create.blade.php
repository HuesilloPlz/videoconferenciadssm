<div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form id="FormCreate" action="api/administrator/videoconferences" method="post">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Video Conferencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" id="user_id" name="user_id">
          <input type="hidden" class="form-control" name="end_date" id="end_date" >
            <div class="form-row">
              <div class="col-4">
                <label>Fecha</label>
                <input type="text" class="form-control" name="start_date" id="start_date" >
              </div>
              <div class="col-4">
                <label>Hora de Inicio</label>
                <input type="text" class="form-control" name="start_time" id="start_time" >
              </div>
              <div class="col-4">
                <label>Hora de Termino</label>
                <input type="text" class="form-control" name="end_time" id="end_time" >
              </div>
            </div>
            <hr>
            <div class="form-row">
              <div class="col-6">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="title" >
              </div>
              <div class="col-6">
                <label>Cantidad de personas</label>
                <input type="text" class="form-control" name="people_cant" id="people_cant" >
              </div>
            </div>
            <hr>
            <div class="form-row">            
              <label>Descripcion</label>
              <textarea type="text" class="form-control" name="descriptions" id="descriptions" cols="30" rows="5"></textarea>
            </div>           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
    </form>
  </div>
</div>