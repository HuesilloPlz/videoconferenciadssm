<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>

    <link rel="stylesheet" href="/assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link href='/assets/fullcalendar/packages/core/main.min.css' rel='stylesheet' />
    <link href='/assets/fullcalendar/packages/daygrid/main.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="/assets/fullcalendar/packages/timegrid/main.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">

</head>
<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg  navbar-dark bg-primary">   
    <b><label id="username" style="color:white;"></label></b>    
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="inicio"> <b>VIDEOCONFERENCIA DSSM</b> <span class="sr-only">(current)</span></a>
                </li>     
                <li class="nav-item active">
                    <a class="nav-link" href="mantenedor"> Mantenedor</a>
                </li>  
                <li class="nav-item">
                        <button class="btn btn-danger" id="salir">Salir</button>
                </li>     
            </ul>                        
    </nav>
   <!--  -->
    
    <!-- CONTENT -->        
    <br>
    <div class="container">

        <!-- Toast -->
            <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide='true' data-delay=3000 style="position:absolute;top:1%; left:1%; z-index: 100;">
        <div class="toast-header">
            <img class="rounded mr-2" id="imageToast" alt="..." height="10px" width="10px">
            <strong class="mr-auto">Estado Modificado</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Estado
        </div>
        </div>
        <!--  -->

        <div class="row">            
            <div class="col-12">
                        <div class="card p-4 shadow-lg p-3 bg-white">
                                <div id="calendar"></div>
                        </div>                                                
            </div>
        </div>
    </div>


    
     @include('administrator.create')   
     @include('administrator.changeState')
    <!--  -->

    <!-- SCRIPTS -->
    <script src="/assets/Jquery/jquery-v341.js"></script>
    <script src="/assets/jwt-decode.min.js"></script>
    <script src="/assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script src='/assets/fullcalendar/packages/core/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/core/locales/es.js"></script>
    <script src='/assets/fullcalendar/packages/daygrid/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/timegrid/main.js"></script>
    <script src="/assets/fullcalendar/packages/interaction/main.min.js"></script>
    <script src="/assets/js/administrator.js"></script>   

    <!--  -->
</body>
</html>