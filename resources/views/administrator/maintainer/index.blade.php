<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>

    <link rel="stylesheet" href="/assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link href='/assets/fullcalendar/packages/core/main.min.css' rel='stylesheet' />
    <link href='/assets/fullcalendar/packages/daygrid/main.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="/assets/fullcalendar/packages/timegrid/main.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="/assets/datatable/datatable.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/all.min.css">
    <style>
        table{
            border:1px solid black;
        }
        table th {
            text-align: center;  
            margin: 15px;
            padding: 15px;  
        }
        table td{   
            border:1px solid black;
            text-align: left;           
            margin: 15px;
            padding: 15px;
        }
    </style>
</head>
<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg  navbar-dark bg-primary">       
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="inicio"> VIDEOCONFERENCIA DSSM <span class="sr-only">(current)</span></a>
                </li>     
                <li class="nav-item active">
                    <a class="nav-link" href="mantenedor"> <b>Mantenedor </b></span></a>
                </li>  
                <li class="nav-item">
                        <button class="btn btn-danger" id="salir">Salir</button>
                </li>     
            </ul>                        
    </nav>
   <!--  -->
    
    <!-- CONTENT -->
    <br>
    <div class="container">
        <div class="card">
            <div class="card-body shadow-lg">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalCreate">Agregar Usuario</button>
                <br>
                <br>
                <table id="tableUsers" class="responsive-table display">
                    <thead>
                        <th>Nombre</th>
                        <th>RUN</th>
                        <th>Email</th>
                        <th>Departamento</th>
                        <th colspan=2>Acciones</th>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>

     @include('administrator.maintainer.create')   
     @include('administrator.maintainer.edit')   

    <!--  -->

    <!-- SCRIPTS -->
    <script src="/assets/Jquery/jquery-v341.js"></script>
    <script src="/assets/jwt-decode.min.js"></script>
    <script src="/assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/assets/datatable/datatable.min.js"></script>
    <script src="/assets/fontawesome/js/all.min.js"></script>
    <script src="/assets/js/maintainer.js"></script>
    <!--  -->
</body>
</html>