<div class="modal fade" id="ModalDetailAll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalle de la VideoConferencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">

                <form id="dataEvent">
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" class="form-control" name="end_date" id="end_date" readonly>
                    <div class="form-row">
                        <div class="col-4">
                            <label><b>Fecha</b></label>
                            <input type="text" class="form-control" name="start_date" id="start_date" readonly>
                        </div>
                        <div class="col-4">
                            <label><b>Hora de Inicio</b></label>
                            <input type="text" class="form-control" name="start_time" id="start_time" readonly>
                        </div>
                        <div class="col-4">
                            <label><b>Hora de Termino</b></label>
                            <input type="text" class="form-control" name="end_time" id="end_time" readonly>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-3">
                            <label><b>Titulo</b></label>
                            <input type="text" class="form-control" name="title" id="title" readonly>
                        </div>
                        <div class="col-3">
                            <label><b>Cantidad de Personas</b></label>
                            <input type="numeric" class="form-control" name="people_cant" id="people_cant" readonly>
                        </div>
                        <div class="col-3">
                            <label><b>Responsable</b></label>
                            <input type="text" class="form-control" name="user_id" id="user_id" readonly>
                        </div>
                        <div class="col-3">
                            <label><b>Departamento</b></label>
                            <input type="numeric" class="form-control" name="department" id="department" readonly>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-12">
                            <label><b>Descripcion</b></label>
                            <textarea type="text" class="form-control" name="descriptions" id="descriptions" cols="30" rows="3" readonly></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>