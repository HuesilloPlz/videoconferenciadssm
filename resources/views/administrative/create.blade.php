<div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form id="FormCreate" action="api/administrative/videoconferences" method="post">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Video Conferencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" id="user_id" name="user_id">
          <input type="hidden" class="form-control" name="end_date" id="end_date"  required>
          
          <div class="form-row">
            <div class="col-4">
                <label><b>Fecha</b></label>          
                <input type="text" class="form-control" name="start_date" id="start_date"  required>
              </div>
            <div class="col-4">
                <label><b>Hora de Inicio</b></label>
                <input type="text" class="form-control" name="start_time" id="start_time" placeholder="Formato de Tiempo: 00:00" required>
            </div>
            <div class="col-4">
              <label><b>Hora de Termino</b></label>
              <input type="text" class="form-control" name="end_time" id="end_time" placeholder="Formato de Tiempo: 00:00" required>
            </div>            
          </div>
          <hr>
          <div class="form-row">
            <div class="col-6">
                <label><b>Titulo</b></label>
                <input type="text" class="form-control" name="title" id="title"  required>
              </div>
              <div class="col-6">
                <label><b>Cantidad de Personas</b></label>
                <input type="numeric" class="form-control" name="people_cant" id="people_cant"  required>
              </div>              
          </div>
          <hr>
          <div class="form-row">
              <div class="col-12">
                <label><b>Descripcion</b></label>
                <textarea type="text" class="form-control" name="descriptions" id="descriptions" cols="30" rows="3" required></textarea>
              </div>
          </div>
                  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
    </form>
  </div>
</div>