<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>

    <link rel="stylesheet" href="/assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link href='/assets/fullcalendar/packages/core/main.min.css' rel='stylesheet' />
    <link href='/assets/fullcalendar/packages/daygrid/main.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="/assets/fullcalendar/packages/timegrid/main.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">

</head>
<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg  navbar-dark bg-primary">  
    <b><label id="username" style="color:white;"></label></b>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="inicio"> <b>VIDEOCONFERENCIA DSSM</b> <span class="sr-only">(current)</span></a>
                </li>      
                <li class="nav-item">
                        <button class="btn btn-danger" id="salir">Salir</button>
                </li>     
            </ul>                        
    </nav>
   <!--  -->
    
    <!-- CONTENT -->
    <br>

    <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Departamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="fullcalendar-tab" data-toggle="tab" href="#fullcalendar" role="tab" aria-controls="profile" aria-selected="false">Todas las VC</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

            <!-- TAB PRINCIPAL -->
                <br>

                <div class="row">
                    <div class="col-3">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <img src="/assets/images/box-green.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Aprobado</label><br>
                                <img src="/assets/images/box-red.png" class="rounded mr-2" height="15px" width="15px">
                                <label>Rechazado</label><br>
                                <img src="/assets/images/box-yellow.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Pendiente</label><br>
                                <img src="/assets/images/box-black.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Suspendida</label>

                            </div>                            
                        </div>
                    </div>            
                    <div class="col-9">
                            <div class="card p-4 shadow-lg p-3 bg-white">
                                <div id="calendar"></div>
                            </div>                                                
                    </div>
                </div>
            
            <!-- ------------- -->

            </div>
            <div class="tab-pane fade" id="fullcalendar" role="tabpanel" aria-labelledby="fullcalendar-tab">

            <!-- TAB SECUNDARIO -->
            <br>
            <div class="row">
                    <div class="col-3">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <img src="/assets/images/box-green.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Aprobado</label><br>
                                <img src="/assets/images/box-red.png" class="rounded mr-2" height="15px" width="15px">
                                <label>Rechazado</label><br>
                                <img src="/assets/images/box-yellow.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Pendiente</label><br>
                                <img src="/assets/images/box-black.jpg" class="rounded mr-2" height="15px" width="15px">
                                <label>Suspendida</label>

                            </div>                            
                        </div>
                    </div>            
                    <div class="col-9">
                            <div class="card p-4 shadow-lg p-3 bg-white">
                                <div id="calendar2"></div>
                            </div>                                                
                    </div>
                </div>

            <!-- -------------- -->

            </div>
        </div>
        
    </div>

     @include('administrative.create')   
     @include('administrative.detail')   
     @include('administrative.alldetail')   


    <!--  -->

    <!-- SCRIPTS -->
    <script src="/assets/Jquery/jquery-v341.js"></script>
    <script src="/assets/jwt-decode.min.js"></script>
    <script src="/assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script src='/assets/fullcalendar/packages/core/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/moment/main.min.js"></script>
    <script src="/assets/fullcalendar/packages/interaction/main.min.js"></script>
    <script src='/assets/fullcalendar/packages/daygrid/main.min.js'></script>
    <script src="/assets/fullcalendar/packages/timegrid/main.js"></script>
    <script src="/assets/fullcalendar/packages/core/locales/es.js"></script>
    <script src="/assets/js/administrative.js"></script>   

    <!--  -->
</body>
</html>