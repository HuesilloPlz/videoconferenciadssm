<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="/assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">
</head>
<style>
    html,
    body {
    height: 100%;
    }

    body {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #D6EAF8;
    }

    .form-signin {
    width: 100%;
    max-width: 400px;
    padding: 15px;
    margin: auto;
    }
    .form-signin .checkbox {
    font-weight: 400;
    }
    .form-signin .form-control {
    position: relative;
    box-sizing: border-box;
    height: auto;
    padding: 10px;
    font-size: 16px;
    }
    .form-signin .form-control:focus {
    z-index: 2;
    }
    .form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    }

</style>
<body class="text-center">
    <form class="form-signin">
        <img class="mb-4" src="/assets/images/Logo-SSM.png" alt="" width="40%" height="40%" style="border-radius: 5%;border:5px solid white">  
        <div class="card">
            <div class="card-body">
                <h1 class="h3 mb-3 font-weight-normal">VideoConferencia DSSM</h1>
                <hr>
                <label for="inputEmail" class="sr-only">E-Mail</label>
                <input type="email" id="email" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="password" class="form-control" placeholder="Password" required>
                    
                <button class="btn btn-lg btn-primary btn-block" type="button" id="login">Aceptar</button>
                <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
            </div>
        </div>   
    </form>

    <script src="/assets/Jquery/jquery-v341.js"></script>
    <script src="/assets/jwt-decode.min.js"></script>
    <script src="/assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/assets/js/login.js"></script>

</body>
</html>