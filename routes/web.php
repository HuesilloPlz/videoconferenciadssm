<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('invitado');
});

Route::get('/login', function () {
    return view('login');
});

Route::group(['prefix'=>'administrativo'],function(){
   
    Route::get('/inicio', function () {return view('administrative.index');});
    
});
Route::group(['prefix'=>'administrador'],function(){
   
    Route::get('/inicio', function () {return view('administrator.index');});
    Route::get('/mantenedor', function () {return view('administrator.maintainer.index');});
    
});
